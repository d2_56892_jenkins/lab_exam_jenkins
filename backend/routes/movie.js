const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/', (request, response) => {
    const query = ` select * from movie `
    
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.delete('/:id', (request, response) => {
    const { id } = request.params

    const query = ` delete from movie where movie_id = ${id} `
    
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.post('/', (request, response) => {
    const { movie_id, movie_title, movie_release_date, movie_time_in_hrs, director_name } = request.params

    const query = ` insert into movie values (${movie_id}, '${movie_title}', '${movie_release_date}', ${movie_time_in_hrs}, '${director_name}') `
    
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.put('/:id', (request, response) => {
    const { id } = request.params
    const { movie_release_date, movie_time_in_hrs } = request.params

    const query = ` update movie set movie_release_date = '${movie_release_date}', movie_time_in_hrs = ${movie_time_in_hrs} where movie_id = ${id} `
    
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router