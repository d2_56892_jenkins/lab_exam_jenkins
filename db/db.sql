create table movie (
    movie_id integer primary key auto_increment,
    movie_title varchar(40),
    movie_release_date date,
    movie_time_in_hrs float,
    director_name varchar(40)
);

insert into movie values (default, 'abc', '1999-10-10', 2.33, 'def');